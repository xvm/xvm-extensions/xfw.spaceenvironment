"""
This file is part of the XFW Space Environment Changer project.

Copyright (c) 2017-2021 XFW Space Environment Changer contributors.

XFW Space Environment Changer is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XFW Space Environment Changer is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import imp
import os
import traceback
import sys
import vfs

class SpaceEnvironmentChanger(object):

    __native = None

    def __init__(self):
        self.__load_native()

    def __load_native(self):
        try:
            if self.__native is None:

                if "python27" not in sys.modules:
                    print "[XFW/SpaceEnvironment][__load_native] libpython was not found"
                    return False

                package_id = 'com.modxvm.xfw.spaceenvironment'
                path_realfs = 'res_mods/mods/xfw_packages/xfw_spaceenvironment/native/xfw_spaceenvironment.pyd'
                path_vfs = 'mods/xfw_packages/xfw_spaceenvironment/native/'

                if os.path.isfile(path_realfs):
                    self.__native = imp.load_dynamic('XFW_SpaceEnvironment', path_realfs)
                else:
                    path_realfs= 'mods\\temp\\%s\\native\\' % package_id
                    vfs.directory_copy(path_vfs, path_realfs)
                    self.__native = imp.load_dynamic('XFW_SpaceEnvironment', os.path.join(path_realfs,'xfw_spaceenvironment.pyd'))

            return True
        except Exception:
            print "[XFW/SpaceEnvironment][__load_native] Error"
            traceback.print_exc()
            print "======================="


#####################################################################
# initialization

g_spaceenvironment = None

try:
    g_spaceenvironment = SpaceEnvironmentChanger()
except Exception:
    print "[XFW/SpaceEnvironment] Error when loading"
    traceback.print_exc()
    print "======================="
