/*
 * This file is part of the XFW Space Environment Changer project.
 *
 * Copyright (c) 2017-2021 XFW Space Environment Changer contributors.
 *
 * XFW Space Environment Changer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Space Environment Changer is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Python.h"
#include <PythonWoT.h>

//CONSTANTS

const char* function_signature = "\x55\x8B\xEC\x83\xEC\x0C\x83\x7D\x08\x00\x56\x0F\x84\x4D\x01\x00";
const char* function_signature_mask = "xxxxxxxxxxxxxxxx";
const unsigned char function_test_offset = 0xA5;
const unsigned char function_test_value = 0xBF;
const unsigned char function_str_findoffset = 0xA6;


/////

DWORD function_addr = 0;
DWORD backup_addr = 0;

constexpr int buffer_size = 256;
char current_env[buffer_size] = {};

/////

void replace_pointer(DWORD originAddress, DWORD value)
{
    char* originFunction = (char*)originAddress;
    DWORD dwProtect = 0;

    VirtualProtect(originFunction, sizeof(value), PAGE_EXECUTE_READWRITE, &dwProtect);
    memcpy(originFunction, &value, sizeof(value));
    VirtualProtect(originFunction, sizeof(value), dwProtect, &dwProtect);
}


static PyObject* Py_SwitchSpaceEnvironment(PyObject* self, PyObject* args)
{
    auto* m_bigworld = PyImport_AddModule("BigWorld");
    if (!m_bigworld) {
        Py_RETURN_FALSE;
    }

    auto* o_customizeenv = PyObject_CallMethod(m_bigworld, "CustomizationEnvironment", nullptr);
    if (!o_customizeenv) {
        Py_RETURN_FALSE;
    }

    char* environment_name = nullptr;
    if (!PyArg_ParseTuple(args, "s", &environment_name)) {
        Py_RETURN_FALSE;
    };

    if (buffer_size < strlen(environment_name)) {
        Py_RETURN_FALSE;
    }

    PyObject* result = nullptr;
    if (strlen(environment_name) == 0) {
        result = PyObject_CallMethod(o_customizeenv, "enable", "i", 0);
    }
    else {
        strcpy(current_env, environment_name);
        replace_pointer(function_addr + function_str_findoffset, reinterpret_cast<DWORD>(current_env));
        result = PyObject_CallMethod(o_customizeenv, "enable", "i", 1);
        replace_pointer(function_addr + function_str_findoffset, backup_addr);
    }

    Py_XDECREF(o_customizeenv);
    return result;
}


bool initialize() {
    WCHAR lpFilename[2048]{};
    GetModuleFileNameW(nullptr, lpFilename, 2048);
    DWORD startpos = (DWORD)GetModuleHandleW(lpFilename);
    DWORD endpos = startpos + WOTPYTHON_GetModuleSize(lpFilename);
    DWORD curpos = startpos;

    //Find
    function_addr = WOTPYTHON_FindFunction(startpos, endpos, &curpos, function_signature, function_signature_mask);
    if (function_addr == 0) {
        return false;
    }

    //Test
    unsigned char* test = (unsigned char*)function_addr + function_test_offset;
    if (test[0] != function_test_value) {
        return false;
    }

    backup_addr = WOTPYTHON_FindStructure(function_addr, function_str_findoffset);
    return true;
}

static PyMethodDef PyMethods[] =
{
    { "set", Py_SwitchSpaceEnvironment, METH_VARARGS, "" },
    {nullptr, nullptr, 0, nullptr},
};

PyMODINIT_FUNC initXFW_SpaceEnvironment()
{
    if (initialize()) {
        Py_InitModule("XFW_SpaceEnvironment", PyMethods);
    }
}
