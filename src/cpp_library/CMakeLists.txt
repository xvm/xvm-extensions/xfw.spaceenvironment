# This file is part of the XFW Performance Monitor project.
#
# Copyright (c) 2017-2021 XFW Performance Monitor contributors.
#
# XFW Performance Monitor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XFW Performance Monitor is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

project(xfw_spaceenvironment)
cmake_minimum_required (VERSION 3.10)

find_package(libpython REQUIRED)

#profiler
set(SOURCES
    "pythonModule.cpp"
)

add_library(xfw_spaceenvironment SHARED "${SOURCES}")
set_target_properties(xfw_spaceenvironment PROPERTIES SUFFIX ".pyd")

target_link_libraries(xfw_spaceenvironment libpython::python27)

install(TARGETS xfw_spaceenvironment
        RUNTIME DESTINATION "." COMPONENT Runtime
        LIBRARY DESTINATION "." COMPONENT Runtime)
