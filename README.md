# XFW Space Environment Selector

[![Build status](https://ci.appveyor.com/api/projects/status/rsbl1j42fqi67bew?svg=true)](https://ci.appveyor.com/project/MikhailPaulyshka/xfw-spaceenvironment)

A little modification for World of Tanks which adds ability to select custom space environment.

## Dependencies

* XFW.Native .wotmod package: https://bitbucket.org/XVM/xfw.native/downloads/

## Example


```python

import XFW_SpaceEnvironment

#Switch to "Customization" environment
XFW_SpaceEnvironment.set('Customization')

#Switch to "hangar_env_v3" environment
XFW_SpaceEnvironment.set('hangar_env_v3')

#Restore to default environment
XFW_SpaceEnvironment.set('')
```