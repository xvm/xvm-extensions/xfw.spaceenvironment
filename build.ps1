# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2021 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

Push-Location $PSScriptRoot
$root = (Get-Location).Path -replace "\\","/"

Import-Module ./build_lib/library.psm1 -Force -DisableNameChecking

function Download-DevelPackage()
{
    $xfwnative_version = (Get-Content "src/meta/version_xfwnative.txt")
    Invoke-WebRequest "https://bitbucket.org/XVM/xfw.native/downloads/com.modxvm.xfw.native_${xfwnative_version}-devel.zip" -OutFile devel.zip
    Expand-Archive -Path ./devel.zip -DestinationPath "$root/output/xfw_devel/"
    Remove-Item -Path "./devel.zip"
}

function Build-CmakeProject ($Name)
{
    Write-Output "  * $Name"

    Remove-Item -Recurse -Path ./output/build/$Name/ -ErrorAction SilentlyContinue | Out-Null
    New-Item -ItemType Directory -Path ./output/build/$Name/ | Out-Null

    $root = (Get-Location).Path -replace "\\","/"

    Push-Location "$root/output/build/$Name/"

    cmake -A Win32 "$root/src/$Name/" -DCMAKE_INSTALL_PREFIX="$root/output/component_cpp/" -DCMAKE_PREFIX_PATH="$root/output/xfw_devel/" | Out-File -FilePath "$root/output/logs/$Name-cmake-config.log"
    if ($LastExitCode -ne 0) {
        Pop-Location
        exit $LastExitCode
    }

    cmake --build . --target INSTALL --config RelWithDebInfo | Out-File -FilePath "$root/output/logs/$Name-cmake-build.log"
    if ($LastExitCode -ne 0) {
        Pop-Location
        exit $LastExitCode
    }

    Pop-Location
}

function Sign-Files()
{
    if ($xfwnative_sign_certfile -eq "") {
        return
    }

    $cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2
    $cert.Import((Resolve-Path -Path $xfwnative_sign_certfile), $xfwnative_sign_certpassword, 'DefaultKeySet')

    $files = Get-ChildItem -Path "$root/output/component_cpp/" -Recurse
    foreach ($file in $files)
    {
        Write-Output "  * $file"
        Set-AuthenticodeSignature -FilePath $file.FullName -Certificate $cert -TimestampServer $xfwnative_sign_timestamp | Out-Null
    }
}

function Build-Python()
{
    Build-PythonFile -FilePath "./src/python_package/__empty__.py"  -OutputDirectory "./output/component_python/res/mods/xfw_packages/xfw_spaceenvironment/" -OutputFileName "__init__.pyc"
    Build-PythonFile -FilePath "./src/python_package/__init__.py"  -OutputDirectory "./output/component_python/res/mods/xfw_packages/xfw_spaceenvironment/python/"
    Build-PythonFile -FilePath "./src/python_package/vfs.py"  -OutputDirectory "./output/component_python/res/mods/xfw_packages/xfw_spaceenvironment/python/"
    Build-PythonFile -FilePath "./src/python_loader/mod_xfw_spaceenvironment.py"  -OutputDirectory "./output/component_python/res/scripts/client/gui/mods/"
}

function Build-Deploy()
{
    $version = Get-Content "./src/meta/version.txt"
    #WOTMOD

    Copy-Item -Path "./output/component_python/res/" -Destination "./output/wotmod/res" -Force -Recurse
    New-Item -Path "./output/wotmod/res/mods/xfw_packages/xfw_spaceenvironment/native/" -ItemType Directory -ErrorAction SilentlyContinue | Out-Null

    Copy-Item -Path "./output/component_cpp/*.pyd" -Destination "./output/wotmod/res/mods/xfw_packages/xfw_spaceenvironment/native/" -Recurse
    Copy-Item -Path "./output/component_cpp/*.dll" -Destination "./output/wotmod/res/mods/xfw_packages/xfw_spaceenvironment/native/" -Recurse

    Copy-Item -Path "./LICENSE.md"       -Destination "./output/wotmod/LICENSE.md"

    (Get-Content "src/meta/wotmod_meta.xml.in").Replace("{{VERSION}}","${version}") | Set-Content "./output/wotmod/meta.xml"
    (Get-Content "src/meta/xfw_package.json").Replace("{{VERSION}}","${version}") | Set-Content "./output/wotmod/res/mods/xfw_packages/xfw_spaceenvironment/xfw_package.json"

    Create-Zip -Directory "./output/wotmod/"

    Move-Item "./output/wotmod/output.zip" "./output/deploy/mods/VER/com.modxvm.xfw.spaceenvironment_${version}.wotmod" -Force
}

Remove-Item -Path ./output/  -Force -Recurse -ErrorAction SilentlyContinue
New-Item -ErrorAction SilentlyContinue -ItemType Directory -Path ./output/build/     | Out-Null
New-Item -ErrorAction SilentlyContinue -ItemType Directory -Path ./output/logs/      | Out-Null
New-Item -ErrorAction SilentlyContinue -ItemType Directory -Path ./output/deploy/    | Out-Null
New-Item -ErrorAction SilentlyContinue -ItemType Directory -Path ./output/deploy/mods/VER/ | Out-Null
New-Item -ErrorAction SilentlyContinue -ItemType Directory -Path ./output/xfw_devel/ | Out-Null

Write-Output "Downloading XFW.Native devel package"
Download-DevelPackage
Write-Output ""


Write-Output "Building python"
Build-Python
Write-Output ""

Write-Output "Building project"
Build-CmakeProject -Name cpp_library
Write-Output ""

Write-Output "Building archives"

Build-Deploy

Write-Output ""

Pop-Location
